# Proyecto MVC

Se agrega nuevo proyecto Springboot (usando Maven).

## Instalación

Se debe instalar STS4 (o en su defecto los plugins para eclipse de SpringToolsSuite4) 
- Descargalo desde https://spring.io/tools

se debe instalar MAVEN.
- Descargar MAVEN desde este link: https://maven.apache.org/download.cgi#:~:text=Binary%20zip%20archive-,apache%2Dmaven%2D3.9.6%2Dbin.zip,-apache%2Dmaven%2D3.9.6
- Descomprimir el archivo y moverlo a C:\Programs Files o donde decidas dejarlo
- Crear la variable de entorno M2_HOME con la ruta al MAVEN recien descargado (si lo dejaste en el C:\ deberia ser algo asi C:\apache-maven-3.9.6)
- Agregar el M2_HOME al PATH (ahi mismo donde creaste la variable de entorno). Agrega el siguiente valor: %M2_HOME%\bin
- Guarda los cambios aceptando y cerrando todas las ventanas que abriste.
- Abre una terminal y ejecuta: mvn -v para mostrar los datos base de MAVEN.

Se debe instalar POSTMAN (o alguno similar)
- Decargalo desde https://www.postman.com/downloads/

## Clonar el proyecto
Para clonar el repositorio, utiliza el siguiente comando:

```bash
git clone https://gitlab.com/202405-alkemy-java-6/202405-proy118.git


