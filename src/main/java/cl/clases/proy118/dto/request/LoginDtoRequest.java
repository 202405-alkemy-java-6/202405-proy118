package cl.clases.proy118.dto.request;

import lombok.Getter;

@Getter
public class LoginDtoRequest {

	private String username;
	private String password;
	
}
