package cl.clases.proy118.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaludoDtoResponse {

	private Integer id;
	private String mensaje;
	
}
