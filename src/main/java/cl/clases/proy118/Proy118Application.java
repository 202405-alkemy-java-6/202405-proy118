package cl.clases.proy118;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Proy118Application {

	public static void main(String[] args) {
		SpringApplication.run(Proy118Application.class, args);
	}

}
