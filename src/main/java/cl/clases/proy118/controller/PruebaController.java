package cl.clases.proy118.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.clases.proy118.dto.request.LoginDtoRequest;
import cl.clases.proy118.dto.response.SaludoDtoResponse;


@RestController
@RequestMapping("/prueba")
public class PruebaController {
	
	@GetMapping("/saludar")
	public ResponseEntity<SaludoDtoResponse> obtenerSaludo() {
		
		SaludoDtoResponse saludo = new SaludoDtoResponse();
		
		saludo.setId(1);
		saludo.setMensaje("hola");
		
		return new ResponseEntity<SaludoDtoResponse>(saludo,HttpStatus.OK);
		
	}
	
	@PostMapping("/login")
	public ResponseEntity<SaludoDtoResponse> login(@RequestBody LoginDtoRequest login ) {
		
		SaludoDtoResponse saludo = new SaludoDtoResponse();
		
		
		if(!login.getPassword().equals("1234") || !login.getUsername().equals("admin")) {

			saludo.setId(0);
			saludo.setMensaje("Username o Password incorrectos");
			
			return new ResponseEntity<SaludoDtoResponse>(saludo,HttpStatus.BAD_REQUEST);
		}
			
	
		saludo.setId(1);
		saludo.setMensaje("hola "+ login.getUsername());
		
		return new ResponseEntity<SaludoDtoResponse>(saludo,HttpStatus.OK);
		
	}

}
